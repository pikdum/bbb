import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ScrollContainer from 'react-indiana-drag-scroll'
import './app.css'
import {
  RecoilRoot,
  atom,
  selector,
  useRecoilState,
  useRecoilValue,
} from 'recoil';

const { session } = require('electron').remote;

const filter = {
  urls: ['*://*.donmai.us/*']
};

session.defaultSession.webRequest.onBeforeSendHeaders(filter, (details, callback) => {
  details.requestHeaders['Origin'] = null;
  callback({ requestHeaders: details.requestHeaders })
});

session.defaultSession.webRequest.onHeadersReceived(filter, (details, callback) => {
  let responseHeaders = details.responseHeaders;
  let statusLine = details.statusLine;
  if (details.method === "OPTIONS") {
    responseHeaders = {
      'access-control-allow-origin': '*',
      'access-control-allow-methods': 'GET, POST, OPTIONS',
      'access-control-allow-headers': 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range'
    }
    statusLine = "HTTP/1.1 204";
  }
  callback({ responseHeaders: responseHeaders, statusLine: statusLine })
});

const searchResultsAtom = atom({
  key: 'searchResultsAtom',
  default: []
})

const selectedImageAtom = atom({
  key: 'selectedImageAtom',
  default: {}
})

const imageZoomAtom = atom({
  key: 'imageZoomAtom',
  default: false
})

const ImagePreviews = () => {

  const [searchResults, setSearchResults] = useRecoilState(searchResultsAtom);
  const [selectedImage, setSelectedImage] = useRecoilState(selectedImageAtom);

  const handlePreviewClick = (index) => {
    setSelectedImage(searchResults[index]);
  }

  return (
    <ScrollContainer className="imagePreviews" hideScrollbars={false} horizontal={false}>
    {searchResults.map((result, index) => {
      if (result.preview_file_url) {
        return(<img key={index} onClick={() => handlePreviewClick(index)} src={ result.preview_file_url }/>);
      }
    })}
    </ScrollContainer>
  )
}

const ImageView = () => {

  const [selectedImage, setSelectedImage] = useRecoilState(selectedImageAtom);
  const [imageZoom, setImageZoom] = useRecoilState(imageZoomAtom);

  const toggleZoom = () => {
    setImageZoom(!imageZoom);
  }

  const RenderContent = () => {
    let file_url = selectedImage.file_url;
    let file_ext = selectedImage.file_ext;

    if (file_ext === "zip") {
      file_url = selectedImage.large_file_url;
      file_ext = file_url.split('.').pop();
    }

    if (file_url) {
      if (['mp4', 'webm'].includes(file_ext)) {
        return (<video className={imageZoom ? 'imageZoom' : 'imageFit'} src={file_url} controls loop></video>);
      } else {
        return (<img className={imageZoom ? 'imageZoom' : 'imageFit'} onClick={toggleZoom} src={selectedImage.file_url}></img>);
      }
    } else {
      return (<></>);
    }
  }
  return(
    <ScrollContainer className="imageView" hideScrollbars={false}>
        <RenderContent/>
    </ScrollContainer>
  )
}

const NavBar = () => {
  return(
    <div className="nav">
      <SearchBar/>
    </div>
  )
}

const SearchBar = () => {
 
  const [searchResults, setSearchResults] = useRecoilState(searchResultsAtom);
  const [searchText, setSearchText] = useState("");
  const [autocomplete, setAutocomplete] = useState([]);

  const onSearchTextChange = (event) => {
    setSearchText(event.target.value);
    let currentWord = event.target.value.split(' ').pop();
    if (currentWord.trim() !== "") {
      axios.get("https://safebooru.donmai.us/autocomplete.json", {
        headers: {
          'Accept': 'application/json, text/javascript, */*; q=0.01',
          'X-Requested-With': 'XMLHttpRequest'
        },
        params: {
          "search[query]": event.target.value.split(' ').pop(),
          "search[type]": "tag_query",
          limit: 10
        }
      }).then((response) => {
        setAutocomplete(response.data);
      }).catch((error) => {
        console.log(error);
      })
    } else {
      setAutocomplete([]);
    }

  }

  const handleSearch = (event) => {
    axios.get("https://safebooru.donmai.us/posts.json", {
      params: {
        tags: searchText,
        limit: '100',
        page: 1
      }
    }).then((response) => {
      setSearchResults(response.data);
    }).catch((error) => {
      console.log(error);
      alert(error.response.data.message);
    })
    event.preventDefault();
  }

  return (
    <form onSubmit={handleSearch}>
      <input list="tags" autoComplete="off" type="text" value={searchText} onChange={onSearchTextChange}></input>
      <input type="submit" value="Search"></input>
      <datalist id="tags">
      {autocomplete.map((tag, index) => {
        let currentWord = searchText.split(' ').pop();
        let combined = searchText.slice(0, -currentWord.length) + tag.value;
        return (<option key={index} value={combined}>{tag.label}</option>)
      })}
      </datalist>
    </form>
  )
}

ReactDOM.render(
  <RecoilRoot>
    <NavBar/>
    <ImagePreviews/>
    <ImageView/>
  </RecoilRoot>
  , document.getElementById('root'));
